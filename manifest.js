const {normalise} = require('./normalise')
const {validate} = require('./validate')
const {schema} = require('./schema')
const {generate} = require('./generate')

module.exports.normalise = normalise
module.exports.validate = validate
module.exports.schema = schema
module.exports.generate = generate

module.exports.Manifest =
class Manifest {
  constructor (manifest = []) {
    this.manifest = manifest
    if (!Array.isArray(this.manifest)) {
      throw new TypeError('Manifest must be an Array')
    }
  }

  validate () {
    return validate(this.manifest)
  }

  normalise () {
    return normalise(this.manifest)
  }
}
