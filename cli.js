#!/usr/bin/env node

const { generate } = require('./generate')
const fs = require('fs')
const { relative } = require('path')
const { promisify } = require('util')

async function main () {
  const [,, command, root, output] = process.argv
  if (command === 'generate') {
    let content
    try {
      content = await generate(root)
    } catch (error) {
      console.error(error.message)
      process.exit(1)
    }
    const json = JSON.stringify(content, null, 2)
    await promisify(fs.writeFile)(output, json)
    const outputPath = relative(process.cwd(), output)
    console.log(`Generated the manifest file: ${outputPath}`)
  } else {
    console.log(`Error: command is not recognized: ${command}`)
  }
}

main()
