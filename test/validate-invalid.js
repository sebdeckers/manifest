const test = require('blue-tape')
const {validate} = require('..')

const fixtures = [
  {
    label: 'Must have a push source',
    manifest: [
      {
        get: '/',
        push: {}
      }
    ]
  },
  {
    label: 'Get path must start with slash or globstar',
    manifest: [
      {
        get: {
          glob: 'index.html'
        },
        push: {
          glob: ['/index.html']
        }
      }
    ]
  },
  {
    label: 'Push path must start with slash or globstar',
    manifest: [
      {
        get: {
          glob: '/index.html'
        },
        push: {
          glob: ['index.html']
        }
      }
    ]
  }
]

for (const {label, manifest} of fixtures) {
  test(`Invalid manifest: ${label}`, async (t) => {
    t.throws(() => validate(manifest))
    // validate(manifest)
  })
}
