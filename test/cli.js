const test = require('blue-tape')
const { promisify } = require('util')
const fs = require('fs')
const { exec } = require('child_process')
const { join } = require('path')
const { dir } = require('tmp')

test('check the presence of manifest after running command', async (t) => {
  const temp = await promisify(dir)({ unsafeCleanup: true })
  const root = join(__dirname, 'fixtures/generate')
  const output = join(temp, 'test.json')
  const command = `node -- cli.js generate ${root} ${output}`
  await promisify(exec)(command)
  const actual = await promisify(fs.readFile)(output, 'utf8')
  const fixture = join(__dirname, 'fixtures/cli-generate.json')
  const expected = await promisify(fs.readFile)(fixture, 'utf8')
  t.deepEqual(actual, expected)
})
