const test = require('blue-tape')
const {validate} = require('..')

const fixtures = {
  'Single and multiple get/uri/glob': [
    {get: '/', push: '/bar'},
    {get: ['/'], push: '/bar'},

    {uri: 'https://example.com/', push: '/bar'},
    {uri: ['https://example.com/'], push: '/bar'},

    {glob: '/', push: '/bar'},
    {glob: ['/'], push: '/bar'}
  ],

  'Complex get': [
    {get: {uri: 'https://example.com/'}, push: '**/*'},
    {get: [{uri: 'https://example.com/'}], push: '**/*'},
    {get: {glob: '/'}, push: '**/*'},
    {get: [{glob: '/'}], push: '**/*'}
  ],

  'Complex push': [
    {glob: '/', push: ['/bar']},
    {glob: '/', push: {glob: '/bar'}},
    {glob: '/', push: {glob: '/bar', uri: 'https://foo'}},
    {glob: '/', push: {glob: '/bar', priority: 1}},
    {glob: '/', push: [{glob: '/bar'}, {glob: '/foo'}]}
  ],

  'Shorthand push as array': [
    {get: '/index.html', push: [['/foo', '/bar']]},
    {
      get: '/index.html',
      push: [
        '**/*',
        {glob: '**/*'},
        ['/foo', '/bar'],
        ['https://example.net/', 'https://example.org/'],
        ['/foo', 'https://example.net/', '/bar', 'https://example.org/']
      ]
    }
  ]
}

for (const label of Object.keys(fixtures)) {
  let index = 0
  for (const given of fixtures[label]) {
    index++
    test(`Valid manifest: ${label} (${index})`, async (t) => {
      t.doesNotThrow(() => {
        validate([given])
      })
    })
  }
}
