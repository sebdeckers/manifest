const extglob = require('extglob')

module.exports.extglobFormat =
function extglobFormat (pattern) {
  try {
    extglob(pattern)
  } catch (error) {
    return false
  }
  return true
}
