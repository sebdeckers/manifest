module.exports.schema = {
  $id: '/Manifest',
  type: 'array',
  definitions: {
    uri: {type: 'string', format: 'uri-template'},
    uris: {
      anyOf: [
        {'$ref': '#/definitions/uri'},
        {
          type: 'array',
          minItems: 1,
          uniqueItems: true,
          items: {'$ref': '#/definitions/uri'}
        }
      ]
    },
    glob: {
      type: 'string',
      allOf: [
        {pattern: '^!?(/|\\*\\*)'},
        {format: 'extglob'}
      ]
    },
    globs: {
      anyOf: [
        {'$ref': '#/definitions/glob'},
        {
          type: 'array',
          minItems: 1,
          uniqueItems: true,
          items: {'$ref': '#/definitions/glob'}
        }
      ]
    },
    push: {
      anyOf: [
        {'$ref': '#/definitions/uri'},
        {'$ref': '#/definitions/glob'},
        {
          type: 'object',
          anyOf: [
            {required: ['uri']},
            {required: ['glob']}
          ],
          properties: {
            priority: {type: 'integer', minimum: 1, maximum: 256},
            uri: {'$ref': '#/definitions/uris'},
            glob: {'$ref': '#/definitions/globs'}
          }
        }
      ]
    },
    get: {
      anyOf: [
        {'$ref': '#/definitions/uri'},
        {'$ref': '#/definitions/glob'},
        {
          type: 'object',
          anyOf: [
            {required: ['uri']},
            {required: ['glob']}
          ],
          properties: {
            uri: {'$ref': '#/definitions/uris'},
            glob: {'$ref': '#/definitions/globs'}
          }
        }
      ]
    }
  },
  additionalProperties: false,
  items: {
    type: 'object',
    additionalProperties: false,
    required: ['push'],
    oneOf: [
      {required: ['get']},
      {
        anyOf: [
          {required: ['uri']},
          {required: ['glob']}
        ]
      }
    ],
    properties: {
      uri: {'$ref': '#/definitions/uris'},
      glob: {'$ref': '#/definitions/globs'},
      get: {
        anyOf: [
          {'$ref': '#/definitions/get'},
          {
            type: 'array',
            minItems: 1,
            uniqueItems: true,
            items: {'$ref': '#/definitions/get'}
          }
        ]
      },
      push: {
        anyOf: [
          {'$ref': '#/definitions/push'},
          {
            type: 'array',
            minItems: 1,
            uniqueItems: true,
            items: {
              anyOf: [
                {'$ref': '#/definitions/push'},
                {'$ref': '#/definitions/uris'},
                {'$ref': '#/definitions/globs'}
              ]
            }
          }
        ]
      }
    }
  }
}
